<?php

$info = array(

  // Pre-defined color schemes.
  'schemes' => array(
    '#fffffe,#13618F,#939598,#98160b,#000000' => t('DOM'),
    '#0072b9,#027ac6,#2385c2,#5ab5ee,#494949' => t('Blue Lagoon (Default)'),
    '#464849,#2f416f,#2a2b2d,#5d6779,#494949' => t('Ash'),
    '#55c0e2,#000000,#085360,#007e94,#696969' => t('Aquamarine'),
    '#d5b048,#6c420e,#331900,#971702,#494949' => t('Belgian Chocolate'),
    '#3f3f3f,#336699,#6598cb,#6598cb,#000000' => t('Bluemarine'),
    '#d0cb9a,#917803,#efde01,#e6fb2d,#494949' => t('Citrus Blast'),
    '#0f005c,#434f8c,#4d91ff,#1a1575,#000000' => t('Cold Day'),
    '#c9c497,#0c7a00,#03961e,#7be000,#494949' => t('Greenbeam'),
    '#ffe23d,#a9290a,#fc6d1d,#a30f42,#494949' => t('Mediterrano'),
    '#788597,#3f728d,#a9adbc,#d4d4d4,#707070' => t('Mercury'),
    '#5b5fa9,#5b5faa,#0a2352,#9fa8d5,#494949' => t('Nocturnal'),
    '#7db323,#6a9915,#b5d52a,#7db323,#191a19' => t('Olivia'),
    '#12020b,#1b1a13,#f391c6,#f41063,#898080' => t('Pink Plastic'),
    '#b7a0ba,#c70000,#a1443a,#f21107,#515d52' => t('Shiny Tomato'),
    '#18583d,#1b5f42,#34775a,#52bf90,#2d2d2d' => t('Teal Top'),
  ),

  // Images to copy over.
  'copy' => array(
    'images/menu-collapsed.gif',
    'images/menu-collapsed-rtl.gif',
    'images/menu-expanded.gif',
    'images/menu-leaf.gif',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'TVframe.css',
    'civicpixel.css',
    'drupal6-reference.css',
    /*
    'html-elements.css',
    'layout.css',
    'tinyMCE.css'
    'ie.css',
     */
  ),

  // Coordinates of gradient (x, y, width, height).
 // 'gradient' => array(0, 37, 760, 121),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 2985, 1500),
    /*
    'bottom' => array(0, 575, 2895, 1500),
    'link' => array(107, 533, 41, 23),
     */
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'img/bg.png'                    => array(0,0 , 2985, 50),
    'img/bg-front-erase.png'                    => array(0, 50, 2985, 50),
    'img/bg-profile-erase.png'                    => array(0, 100, 2985, 50),
    'img/bg-project-profile-erase.png'                    => array(0, 150, 2985, 50),
    'img/bg-project-erase.png'                    => array(0, 200, 2985, 50),
    'img/bg-watch-erase.png'                    => array(0, 250, 2985, 50),
    'img/arrows-down.png'                    => array(0, 300, 20, 12),
    'img/arrow-right-invert.png'                    => array(20, 300, 20, 12),
    'img/arrow-right.png'                    => array(40, 300, 20, 12),
    'img/bg-image-144.png'                    => array(60, 300, 402, 259),
    'img/bg-image-147.png'                    => array(462, 300, 402, 259),
    'img/bg-image-151.png'                    => array(864, 300, 402, 259),
    'img/bg-image-220.png'                    => array(1266, 300, 402, 259),
    'img/block-item-157.png'                    => array(0, 559, 314, 8),
    'img/block-item-232.png'                    => array(314, 559, 464, 8),
    'img/block-item-244.png'                    => array(778, 559, 488, 8),
    'img/block-item-339.png'                    => array(1266, 559, 678, 8),
    'img/block-item-488.png'                    => array(1948, 559, 976, 8),
    'img/block-item-545.png'                    => array(0, 567, 1094, 8),
    'img/block-item-comment-fullwidth.png'      => array(1094, 567, 694, 8),
    'img/block-item-comment.png'      => array(1788, 567, 860, 8),
    'img/block-item-244-outline.png'                    => array(1668, 300, 488, 5),
    'img/button-search.png'                    => array(1668, 305, 55, 22),
    /*
    'img/header-project-nosidebar.png'                    => array(0, 575, 727, 5),
    'img/header-page-nosidebar.png'                    => array(0, 580, 734, 4),
    'img/header-full-page.png'                    => array(0, 584, 537, 4),
     */
    /*   'img/calendar-term-134.png'                    => array(1723, 305, 15, 15),*/
    /*
    'images/body.png'                      => array(0, 37, 1, 280),
    'images/bg-bar-white.png'              => array(202, 506, 76, 14),
    'images/bg-tab.png'                    => array(107, 533, 41, 23),
    'images/bg-navigation.png'             => array(0, 0, 7, 37),
    'images/bg-content-left.png'           => array(40, 117, 50, 352),
    'images/bg-content-right.png'          => array(510, 117, 50, 352),
    'images/bg-content.png'                => array(299, 117, 7, 200),
    'images/bg-navigation-item.png'        => array(32, 37, 17, 12),
    'images/bg-navigation-item-hover.png'  => array(54, 37, 17, 12),
    'images/gradient-inner.png'            => array(646, 307, 112, 42),

    'logo.png'                             => array(622, 51, 64, 73),
    'screenshot.png'                       => array(0, 37, 400, 240),
     */
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
