<div class="node"><div class="node-inner">
<!-- Come up with class names to replace these that are only used in the profile and project templates! -->

<div id="content-main">
  <div class="p-main-inner">

<div class="block content-block my-projects">
  <h2 class="title">I am involved in these projects</h2>
  <div class="block-inner">
    <!-- php print views_embed_view('user_projects', 'default', $account->uid ); -->
    <!-- p><!php print $account->content['summary']['groups']['#title'] !></p -->
    <p><?php print $account->content['summary']['groups']['#value'] ?></p>
  </div>
</div><!-- /.block -->


<div class="block content-block rated-projects">
  <h2 class="title">Need a View of projects(?) rated 5 by <?php print $account->name ?></h2>
  <div class="block-inner">
    <p>view not created</p>
  </div>
</div><!-- /.block -->

<?php
$mystring = views_embed_view('user_blog_posts', 'default', $account->uid );
$findme   = 'VdBhH9adqdqReGHs';

$pos = strpos($mystring, $findme);

// Note our use of ===.  Simply == would not work as expected
// because the position of 'a' was the 0th (first) character.
?>
<!-- if pos = false, the view has content -->
<?php if ($pos === false): ?>
  <div class="block content-block recent-posts">
    <h2 class="title"><?php print $account->name ?>'s most recent blog posts</h2>
    <div class="block-inner">
      <p class="block-note">Post an event by clicking <strong>Create Project Event</strong> in the <strong>My Project Tools</strong> block on the right.</p>
      <?php print views_embed_view('user_blog_posts', 'default', $account->uid ); ?>
    </div>
  </div><!-- /.content-block -->
<?php elseif ($account->uid == $user->uid): ?>
  <div class="block content-block recent-posts">
    <h2 class="title">You have no Blog posts</h2>
    <div class="block-inner">
      <p>You can only create blog posts on projects you are a part of. <a href="/projects/my">View your projects</a> and choose one to create new blog posts.</p>
    </div>
  </div><!-- /.content-block -->
<?php endif; ?>

  </div> <!-- /.p-main-inner -->
</div> <!-- /#content-main -->


<div id="content-sidebar">
  <div class="block user-image-block">
    <h2 class="title"><?php print $account->name ?></h2>
    <div class="content">
      <?php print $account->content['user_picture']['#value'] ?>
    </div> <!-- /.content -->
  </div> <!-- /.block -->


  <div class="block">
    <h2 class="title">User Info</h2>
    <div class="content">


<!-- PERSONAL INFORMATION REMOVED HERE -->
<!-- I can't seem to further subdivide this section!!! -->

<!-- ?php print $account->content['Station Use Information']['Station Use Information']['#value'] ? -->

<!--
    <dt>User Name</dt>
    <dd><?php print $account->name ?></dd>
    <dt>Real Name</dt>
<dt><?php print $account->content['summary']['member_for']['#title'] ?></dt>
<dd><?php print $account->content['summary']['member_for']['#value'] ?></dd>
    <dt>Age</dt>
    <dt>Location</dt>
  </dl>
-->

<!-- END PERSONAL INFORMATION -->


<p><?php print "User Name: <strong>" .$account->name."</strong>"; ?></p>
<p><?php print "Member for: <strong>" .$account->content['summary']['member_for']['#value']."</strong>"; ?></p>

    </div> <!-- /.content -->
  </div> <!-- /.block -->


<?php if ($account->name == $user->name or $account->content['Skills']): ?>
  <div class="block content-block my-skills">
    <h2 class="title">My Skills</h2>
    <div class="block-inner">

<ul id="MySkills">

<?php if ($account->content['Skills']['profile_graphics']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Graphics</span>
  <?php $skill_level = $account->content['Skills']['profile_graphics']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_writer']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Writer</span>
  <?php $skill_level = $account->content['Skills']['profile_writer']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_talent']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Talent</span>
  <?php $skill_level = $account->content['Skills']['profile_talent']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_editor']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Editor</span>
  <?php $skill_level = $account->content['Skills']['profile_editor']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_lighting']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Lighting</span>
  <?php $skill_level = $account->content['Skills']['profile_lighting']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
     <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_audio']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Audio</span>
  <?php $skill_level = $account->content['Skills']['profile_audio']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_crew']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Crew</span>
  <?php $skill_level = $account->content['Skills']['profile_crew']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_camera']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Camera</span>
  <?php $skill_level = $account->content['Skills']['profile_camera']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_director']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Director</span>
  <?php $skill_level = $account->content['Skills']['profile_director']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

<?php if ($account->content['Skills']['profile_producer']['#value']): ?>
<li class="MySkill clear-block">
  <span class="MySkill-name">Producer</span>
  <?php $skill_level = $account->content['Skills']['profile_producer']['#value'] ?>
  <?php $skill_level_name = substr($skill_level, 3) ?>
  <span class="MySkill-level skill-level-<?php print $skill_level{0};?>">
    <span class="l-graph"></span>
    <span class="l-name">
      <?php if ($skill_level_name): ?>
        <?php print $skill_level_name ?>
      <?php else: ?>
<a href="/user/<?php print $user->uid ?>/edit/Skills">Edit Skill levels</a>
      <?php endif; ?>
    </span>
  </span>
</li>
<?php endif; ?>

</ul>

    </div><!-- /.block-inner -->
  </div><!-- /.content-block -->
<?php endif; ?>


<?php if ($account->content['Skills']['profile_volunteer']['#value'] and $account->content['Skills']['profile_volunteer_interests']['#value']): ?>
  <div class="block content-block">
    <h2 class="title">I want to volunteer for:</h2>
    <div class="block-inner">
<p><?php print $account->content['Skills']['profile_volunteer_interests']['#value'] ?></p>
    </div><!-- /.block-inner -->
  </div><!-- /.content-block -->
<?php elseif ($account->content['Skills']['profile_volunteer']['#value']): ?>
  <div class="block content-block">
    <h2 class="title">I want to volunteer</h2>
    <div class="block-inner">
<p>View everyone that <?php print l('wants to volunteer','profile/profile_volunteer') ?></p>
    </div><!-- /.block-inner -->
  </div><!-- /.content-block -->
<?php endif; ?>

</div> <!-- /#content-sidebar -->


</div></div> <!-- /node-inner, /node -->
