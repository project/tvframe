<div class="node <?php print $node_classes; ?>" id="node-<?php print $node->nid; ?>"><div class="node-inner">

  <?php if ($page == 0): ?>
    <h2 class="title">
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>

  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>

  <?php if ($picture) print $picture; ?>

  <?php if ($submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' in ') . $terms; ?></div>
  <?php endif; ?>

  <div class="content">
    <?php print $content; ?>
  </div>

  <div class="registration">
    <?php if($user->uid) { ?>
      <?php print $node->field_registration_link[0]['value']; ?>
    <?php } else { ?>
      <strong>In order to register for a class, you must first login or register for an account on the website. 
      <a href="/user">Click here to login or register.</a></strong>
    <?php } ?>
  </div>
</div></div> <!-- /node-inner, /node -->