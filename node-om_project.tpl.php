<?php
global $user;
global $blog_empty;
global $wiki_empty;
global $event_empty;
global $crew_empty;
global $members_empty;

if($node->content['og_mission']['#value']) {
  $desc_empty = 0;
} else {
  $desc_empty = 1;
}

if($node->field_public_om_credits[0]['view']) {
  $credit_empty = 0;
} else {
  $credit_empty = 1;
}

$adminof	= $user->og_groups;
$current	= $node->nid;
foreach ($adminof as $key => $data) {
  if ($key == $current) {
    $isadminof = 1;
  }
};

if ($node->field_logo[0]['filepath']) {
  $img_empty = 0;
} else {
  $img_empty = 1;
}


$blog_content	= views_embed_view('project_views', 'block_3', $node->nid );
$wiki_content	= views_embed_view('project_views', 'block_4', $node->nid );
$event_content	= views_embed_view('project_views', 'block_5', $node->nid );
$crew_content	= views_embed_view('project_views', 'block_6', $node->nid );
$members		= views_embed_view('og_members_block', 'block_3', $node->nid );
$shows		= views_embed_view('project_views', 'block_7', $node->nid );

$lquery = "gids[]=".$node->nid;

?>


<div class="node <?php print $node_classes; ?>" id="node-<?php print $node->nid; ?>"><div class="node-inner">

<!-- if teaser view -->
  <?php if ($page == 0): ?>
    <h2 class="title">
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>

<!-- if page view -->
  <?php if (!$page == 0): ?>

<!-- open containing div -->
<div id="node-sidebar-container" class="clear-block">

  <!-- open main area -->
  <div id="content-main">

<!-- ****** ****** ****** Begin Project Description ****** ****** ****** -->

<?php print $node_region; ?>

<div class="block content-block">
  <h2 class="title">Project Description</h2>
  <div class="block-inner">
  <?php if (!$desc_empty): ?>
	<?php print $node->content['og_mission']['#value']; ?>
  <?php elseif ($node->og_description): ?>
	<?php print $node->og_description; ?>
  <?php else: ?>
	<p class="block-note">If you have permissions to edit this project you'll see an edit tab above. Click on the edit tab to create a description.</p>
	<p>This project has no description.</p>
  <?php endif; ?>
  </div>
</div> <!-- /.block -->

<!-- elseif you are the project owner -->
<!-- add your shit! -->
  

<!-- ****** ****** ****** Begin Project Crew Requests ****** ****** ****** -->
  <?php if (!$crew_empty): ?>
    <div class="block content-block project-crew-requests">
      <h2 class="title">Project Crew Requests</h2>
      <div class="block-inner">
        <p class="block-note">Post a new request by clicking <strong>Create Crew Requests</strong> in the <strong>My Project Tools</strong> block on the right.</p>
        <?php print $crew_content; ?>
      </div>
    </div><!-- /.content-block -->
  <?php endif; ?>


<!-- ****** ****** ****** Begin Project Event Postings ****** ****** ****** -->
<?php if (!$event_empty): ?>
    <div class="block content-block project-events">
      <h2 class="title">Project Events</h2>
      <div class="block-inner">
        <p class="block-note"><?php print l('Post a project event here','node/add/project-blog', array('query' => $lquery)); ?> or in the <strong>My Project Tools</strong> block on the right.</p>
        <?php print $event_content; ?>
      </div>
    </div><!-- /.content-block -->
<?php endif; ?>

<!-- ****** ****** ****** Begin Project Blog Postings ****** ****** ****** -->
<?php if (!$blog_empty): ?>
    <div class="block content-block project-blogs">
      <h2 class="title">Project Blog Postings</h2>
      <div class="block-inner">
		<?php if ($logged_in): ?>
          <p class="block-note"><?php print l('Create project bog post here','node/add/project-blog', array('query' => $lquery)); ?> or in the <strong>My Project Tools</strong> block on the right.</p>
		<?php endif; ?>
        <?php print $blog_content; ?>
      </div>
    </div><!-- /.content-block -->
<?php endif; ?>


<!-- ****** ****** ****** Begin wiki ****** ****** ****** -->
<?php if ($isadminof || !$wiki_empty): ?>
    <div class="block content-block project-wiki">
      <h2 class="title">Project Wiki</h2>
      <div class="block-inner">
        <?php if ($isadminof): ?>
          <p class="block-note">You can do collaborative project work here or place notes for the project contributors to see. Make collaborative scripts, to do lists, lists of equipment you have available for the project, or any other type of collaborative documentation.</p>
          <p><?php print l('create a project wiki','node/add/project-wiki', array('query' => $lquery)); ?></p>
        <?php endif; ?>
        <?php print $wiki_content; ?>
      </div>
    </div><!-- /.content-block -->
<?php endif; ?>

<!-- ****** ****** ****** Begin Project Credits ****** ****** ****** -->
<?php if (!$credit_empty): ?>
  <div class="block content-block project-credits">
    <h2 class="title">Project Credits</h2>
    <div class="block-inner">
	  <?php print $node->field_public_om_credits[0]['view']; ?>
    </div>
  </div><!-- /.content-block -->
<?php endif; ?>

<!-- ****** ****** ****** Begin Project Status ****** ****** ****** -->
<?php if ($isadminof): ?>
  <div class="block content-block project-status">
    <h2 class="title">Project Status</h2>
    <div class="block-inner">
<table>

  <tr class="odd">
    <td>Description</td>
    <?php if ($desc_empty): ?>
      <td>The full description is missing<br />(short description is being used)</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('add full description on edit page','node/'.$node->nid.'/edit'); ?></td>
    <?php else: ?>
      <td>This project has a description</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('edit this page','node/'.$node->nid.'/edit'); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="even">
    <td>Project Image</td>
    <?php if ($img_empty): ?>
      <td>This project has no image/logo</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('upload an image on edit page','node/'.$node->nid.'/edit'); ?></td>
    <?php else: ?>
      <td>This project has an image/logo</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('change the image on edit page','node/'.$node->nid.'/edit'); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="odd">
    <td>Blog</td>
    <?php if ($blog_empty): ?>
      <td>No blog posts exist</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('create a project blog','node/add/project-blog', array('query' => $lquery)); ?></td>
    <?php else: ?>
      <td>At least one blog post exists</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('edit project blog','node/add/project-blog', array('query' => $lquery)); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="even">
    <td>Wiki</td>
    <?php if ($wiki_empty): ?>
      <td>The wiki hasn't been started</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('create a project wiki','node/add/project-wiki', array('query' => $lquery)); ?></td>
    <?php else: ?>
      <td>Wiki content exists</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('edit project wiki','node/add/project-wiki', array('query' => $lquery)); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="odd">
    <td>Events</td>
    <?php if ($event_empty): ?>
      <td>This project has no events</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('create a project event','node/add/project-event', array('query' => $lquery)); ?></td>
    <?php else: ?>
      <td>This project has at least one event</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('edit project event','node/add/project-event', array('query' => $lquery)); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="even">
    <td>Crew Requests</td>
    <?php if ($crew_empty): ?>
      <td>This project has no crew requests</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php print l('create a project event','node/add/om-crew-request', array('query' => $lquery)); ?></td>
    <?php else: ?>
      <td>This project has crew requests</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php print l('edit a project event','node/add/om-crew-request', array('query' => $lquery)); ?></td>
    <?php endif; ?>
  </tr>

  <tr class="odd">
    <td>Members</td>
    <?php if ($members_empty): ?>
      <td>This project has no members</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/x.png" alt="" /></td>
      <td><?php
      	$thispath = "og/users/".$node->nid."/add_user";
      	print l('add members',$thispath); ?></td>
    <?php else: ?>
      <td>This project has members</td>
      <td><img src="http://www.denveropenmedia.org/sites/all/themes/TVframe/img/check.png" alt="" /></td>
      <td><?php
      	$thispath = "og/users/".$node->nid."/faces";
      	print l('view members',$thispath)."<br />";
      	$thispath = "og/users/".$node->nid."/add_user";
      	print l('add members',$thispath); ?></td>
    <?php endif; ?>
  </tr>

</table>
    </div>
  </div><!-- /.content-block -->
<?php endif; ?>

  <?php endif; ?><!-- end page view if -->



  <?php if ($unpublished): ?>
    <div class="unpublished"><?php print t('Unpublished'); ?></div>
  <?php endif; ?>

  <?php if ($picture) print $picture; ?>

  <?php if ($submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' in ') . $terms; ?></div>
  <?php endif; ?>

<!-- content variable removed here -->

  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

<!-- if page view -->
  <?php if (!$page == 0): ?>
  </div> <!-- /#content-main -->

  <!-- open sidebar -->
  <div id="content-sidebar">

    <!-- sidebar content -->
<div id="project-logo">
<?php if ($imgempty): ?>
  <img src="<?php print base_path() . path_to_theme() ?>/img/project-not-uploaded.png" alt="default project logo" />

  <?php if ($isadminof) {
    print l('upload an image on the edit page','node/'.$node->nid.'/edit');
  }; ?>

<?php else: ?>
  <?php if ($node->field_logo[0]['filepath']) {
  print theme('imagecache', '210 pixels wide scale', $node->field_logo[0]['filepath']); 
  } ?>
<?php endif; ?>
</div> <!-- /#project-logo -->

<!-- ****** ****** ****** Begin Project Info ****** ****** ****** -->
<?php if (!$desc_empty && $node->og_description || $node->field_om_public_contact_info[0]['value'] || $node->field_om_public_email[0]['value'] || $node->field_om_public_url[0]['value']): ?>

<div class="block content-block project-info">
  <h2 class="title">Project Info</h2>
    <div class="block-inner">
      <div id="project-short-desc">

      <?php if (!$desc_empty && $node->og_description): ?>
        <?php print $node->og_description; ?>
      <?php endif; ?>
      </div> <!-- /#project-short-desc -->

      <?php if ($node->field_om_public_contact_info[0]['value']): ?>
        <?php $contact_field = $node->field_om_public_contact_info[0]['value']; ?>
        <p>contact info: <strong><?php print $contact_field; ?></strong></p>
      <?php endif; ?>

      <?php if ($node->field_om_public_email[0]['value']): ?>
        <?php $contact_field = $node->field_om_public_email[0]['value']; ?>
        <p>email: <strong><?php print $contact_field; ?></strong></p>
      <?php endif; ?>

      <?php if ($node->field_om_public_url[0]['value']): ?>
        <?php $contact_field = $node->field_om_public_url[0]['value']; ?>
        <p><strong><a href="<?php print $contact_field; ?>"><?php print $contact_field; ?></a></strong></p>
      <?php endif; ?>

  </div> <!-- /.block-inner -->
</div> <!-- /.block-260 -->

<?php endif; ?>


<!-- ****** ****** ****** Begin Contributors ****** ****** ****** -->
<?php if ($user->uid): ?>
  <div class="block content-block project-admin">
    <h2 class="title">Contributors</h2>
    <div class="block-inner">
      <?php print views_embed_view('og_members_block', 'block_2', $node->nid ); ?>
    </div> <!-- /.block-inner -->
  </div> <!-- /.block-260 -->
<?php endif; ?>


<!-- ****** ****** ****** Begin Random Members ****** ****** ****** -->

<?php if (!$members_empty): ?>
  <div class="block content-block project-members">
    <h2 class="title">Random Members</h2>
    <div class="block-inner">
      <?php print views_embed_view('og_members_block', 'block_3', $node->nid ); ?>
    </div> <!-- /.block-inner -->
  </div> <!-- /.block-260 -->
<?php endif; ?>

<!-- ****** ****** ****** Begin All Shows ****** ****** ****** -->


  <div class="block content-block project-shows">
    <h2 class="title">All Shows</h2>
    <div class="block-inner">
      <?php print views_embed_view('project_views', 'block_7', $node->nid ); ?>
    </div> <!-- /.block-inner -->
  </div> <!-- /.block-260 -->



    <!-- END sidebar content -->

  </div> <!-- /#content-sidebar -->


</div> <!-- /#node-sidebar-container -->
  <?php endif; ?>



</div></div> <!-- /node-inner, /node -->
