<?php

/**
 * @file
 *
 * OVERRIDING THEME FUNCTIONS
 *
 * The Drupal theme system uses special theme functions to generate HTML output
 * automatically. Often we wish to customize this HTML output. To do this, we
 * have to override the theme function. You have to first find the theme
 * function that generates the output, and then "catch" it and modify it here.
 * The easiest way to do it is to copy the original function in its entirety and
 * paste it here, changing the prefix from theme_ to TVframe_. For example:
 *
 *   original: theme_breadcrumb()
 *   theme override: TVframe_breadcrumb()
 *
 * where TVframe is the name of your sub-theme. For example, the zen_classic
 * theme would define a zen_classic_breadcrumb() function.
 *
 * If you would like to override any of the theme functions used in Zen core,
 * you should first look at how Zen core implements those functions:
 *   theme_breadcrumbs()      in zen/template.php
 *   theme_menu_item_link()   in zen/template-menus.php
 *   theme_menu_local_tasks() in zen/template-menus.php
 */


/*
 * Add any conditional stylesheets you will need for this sub-theme.
 *
 * To add stylesheets that ALWAYS need to be included, you should add them to
 * your .info file instead. Only use this section if you are including
 * stylesheets based on certain conditions.
 */

/* -- Delete this line if you want to use and modify this code
// Example: optionally add a fixed width CSS file.
if (theme_get_setting('TVframe_fixed')) {
  drupal_add_css(path_to_theme() . '/layout-fixed.css', 'theme', 'all');
}
// */


/**
 * Implementation of HOOK_theme().
 */
function TVframe_theme(&$existing, $type, $theme, $path) {
  return zen_theme($existing, $type, $theme, $path);
}

/**
 * Override or insert PHPTemplate variables into all templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function TVframe_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert PHPTemplate variables into the page templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("page" in this case.)
 */
function TVframe_preprocess_page(&$vars, $hook) {
      // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}
// */

/**
 * Override or insert PHPTemplate variables into the node templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("node" in this case.)
 */
/* -- Delete this line if you want to use this function
function TVframe_preprocess_node(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert PHPTemplate variables into the comment templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function TVframe_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert PHPTemplate variables into the block templates.
 *
 * @param $vars
 *   A sequential array of variables to pass to the theme template.
 * @param $hook
 *   The name of the theme function being called ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function TVframe_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */


/**
 * The rel="nofollow" attribute is missing from anonymous users' URL in Drupal 6.0-6.2.
 */
/* -- Delete this line if you want to use this function
function TVframe_username($object) {

  if ($object->uid && $object->name) {
    // Shorten the name when it is too long or it will break many tables.
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) . '...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/' . $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    // Sometimes modules display content composed by people who are
    // not registered members of the site (e.g. mailing list or news
    // aggregator modules). This clause enables modules to display
    // the true author of the content.
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }

    $output .= ' (' . t('not verified') . ')';
  }
  else {
    $output = variable_get('anonymous', t('Anonymous'));
  }

  return $output;
}
// */

function dom_menu_body_class() {
  $trail = menu_tree_page_data('primary-links');

  foreach($trail as $item => $info) {
    if ($info['link']['in_active_trail'] == 1 && $info['link']['plid'] == 0) {
      $extra_class = ' menu-section-'.$info['link']['mlid'];
      break;
    }
  }

  return $extra_class;
}

/* Allow placement / or non-placement of comments in node
 * To insert comments in node.tpl.php use $comments and/or
 * $comment_form */ 
function phptemplate_preprocess_page(&$vars) {
  $vars['comments'] = $vars['comment_form'] = '';
  if (module_exists('comment') && isset($vars['node'])) {
    $vars['comments'] = comment_render($vars['node']);
    $vars['comment_form'] = drupal_get_form('comment_form', array('nid' => $vars['node']->nid));
  }
}

function phptemplate_preprocess_node(&$vars) {
  if($vars['node']->type == 'livestream'){
    $vars['node']->comment = 0;
  }
}


/* This would add the user login template if i got it to work */
/* if i change mytheme to TVframe, it conflicts with the prior use on line 49 */
/* from http://drupal.org/node/19855 */
function mytheme_theme() {
  return array(
    'user_login_block' => array(
      'template' => 'user_login',
      'arguments' => array('form' => NULL),
    ),
    // other theme registration code...
  );
}

function mytheme_preprocess_user_login_block(&$variables) {
  $variables['intro_text'] = t('This is my awesome login form');
  $variables['rendered'] = drupal_render($variables['form']);
}


/**
 * Theme function for shoutbox posts.
 *
 * @param shout
 *   The shout to be themed.
 * @param links
 *   Links of possible actions that can be performed on this shout
 *   by the current user.
 * @param $alter_row_color
 *   Whether or not to alternate the row color for posts.  Should be set to
 *   FALSE for the page view.
 */
function phptemplate_shoutbox_post($shout, $links = array(), $alter_row_color=TRUE) {
  // Get the registered username of the person who posted the shout.
  if ($shout->uid > 0) {
    $user = user_load(array("uid" => $shout->uid));
    $shout->username = $user->name;
  }
  else {
    $shout->username = 'an anonymous user';
  }

  // BUGBUG strstr returns from http:// till end
  // we should use that instead of full url.
  if (strstr($shout->url, "http://")) {
    $shout->url = '<a href="'. $shout->url .'" target="_blank">'. $shout->nick .'</a>';
  }
  else {
    $shout->url = $shout->nick;
  }

  if ($links) {
    foreach ($links as $link) {
      $linkattributes = $link['linkattributes'];
      $link_html = '<img src="'. $link['img'] .'"  width="'. $link['img_width'] .'" height="'. $link['img_height'] .'" alt="'. $link['title'] .'" class="shoutbox-imglink">';
      $link_url = 'shoutbox/'. $shout->shout_id .'/'. $link['action'];
      $img_links = l($link_html, $link_url, array('html' => TRUE)) . $img_links;
    }
  }

  $title = 'Posted '. format_date($shout->created, 'custom', 'm/d/y') .' at '. format_date($shout->created, 'custom', 'h:ia') .' by '. $shout->username;
  $shout_classes="shoutbox-msg ";
  if ($alter_row_color) {
    $shout_classes .= (($shout->color) ? ("shout-odd ") : ("shout-even "));
  }

  if ($shout->moderate == 1) {
    $shout_classes .= "shoutbox-unpublished ";
    $shout->shout .= t("(This shout is waiting for approval by a moderator.)");
  }
  return "<div class=\" $shout_classes \" title=\"$title\">$img_links<div class=\"shouter\">$shout->url:</div><div class=\"shouted\">$shout->shout</div></div>\n";
}

function TVframe_preprocess_node(&$variables, $hook) {
  $variables['node_region'] = theme('blocks', 'node_region');
}
